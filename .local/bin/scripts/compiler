#!/bin/sh

# This script will compile or run another finishing operation on a document. I
# have this script run via vim.
#
# Compiles .tex. groff (.mom, .ms), .rmd, .md, .org.  Opens .sent files as sent
# presentations. Runs scripts based on extention or shebang.
#
# Note that .tex files which you wish to compile with XeLaTeX should have the
# string "xelatex" somewhere in a comment/command in the first 5 lines.


file=$(readlink -f "$1")
dir=${file%/*}
base="${file%.*}"
ext="${file##*.}"

cd "$dir" || exit 1

textype() { \
	command="pdflatex"
	( head -n5 "$file" | grep -qi 'xelatex' ) && command="xelatex"
	$command --output-directory="$dir" "$base" &&
	grep -qi addbibresource "$file" &&
	biber --input-directory "$dir" "$base" &&
	$command --output-directory="$dir" "$base" &&
	$command --output-directory="$dir" "$base"
}

case "$ext" in
	# Try to keep these cases in alphabetical order.
	[0-9]) preconv "$file" | refer -PS -e | groff -mandoc -T pdf > "$base".pdf ;;
	# c) cc  -std=c11  -g -static -Wall -Wextra -pedantic "$file" -o "$base" && "$base" ;;
	c) cc  -std=c11  -g -O2 -Wall -Wextra -pedantic "$file" -o "$base" && "$base" ;
     rm "$base";;
	lhs|hs) ghc "$file" -o "$base" && "$base";;
	# lhs|hs) ghc "$file" && "$base" ;;
	rs) rustc "$file" -o "$base" && "$base" ;;
	cpp)
       if [  -e input.txt ] ; then
          g++ -std=c++17 -g  -O2 -Wall -Wextra "$file" -o "$base" && "$base" <./input.txt > output.txt 
       else
          g++ -std=c++17 -g  -O2 -Wall -Wextra "$file" -o "$base" && "$base" ;
       fi
          rm "$base"
       ;;
	cs) mcs "$file" -o "$base" && mono "$base";;
	md) pandoc --pdf-engine=xelatex "$file"   -o "$base".pdf;;
	rmd)Rscript -e "rmarkdown::render('$file')";;
	d) ldc2 "$file" &&  "$base" ;;
	# d) dmd  -O -of="$base" "$file" && "$base" ;;
	fsx) fsharpc  --nologo    "$file" -o "$base"  && mono "$base";;
	ml)ocamlopt "$file" -o "$base" && "$base";;
	go) go build  "$file" && "$base";;
	h) sudo make install ;;
	v) v "$file" -o "$base" && "$base" ;;
	java) javac -d classes "$file" && java -cp classes "${1%.*}" ;;
	# nim) nim --verbosity:0 --hints:off  c "$file" && "$base";;
	nim) nim   --hints:off  c "$file" && "$base";;
	m) octave "$file" ;;
	py) python3 "$file" ;;
	ex|exs)elixir "$file" ;;
	sent) setsid -f sent "$file" 2>/dev/null ;;
	tex) pdflatex --latex-engne=xelatex "$file" ;;
  zig) zig run "$file";;
	*) sed -n '/^#!/s/^#!//p; q' "$file" | xargs -r -I % "$file" ;;
esac
